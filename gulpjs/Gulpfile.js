//require('es6-promise').polyfill();


// Load plugins

const { task, series, src, dest, } = require('gulp');

// autoprefixer = require('gulp-autoprefixer'),

const autoprefixer = require('autoprefixer'),

    minifycss = require('gulp-minify-css'),

    jshint = require('gulp-jshint'),

    //uglify = require('gulp-uglify'),

    imagemin = require('gulp-imagemin'),

    rename = require('gulp-rename'),

    concat = require('gulp-concat'),

    notify = require('gulp-notify'),

    cache = require('gulp-cache'),

    //browsersync = require('browser-sync'),

    sourcemaps = require('gulp-sourcemaps'),

    babel = require('gulp-babel'),

    exec = require('gulp-exec'),

    replace = require('gulp-string-replace'),

    del = require('del');

const autoprefixerOptions = {

    browsers: ['> 5%']

};

const revAll = require('gulp-rev-all')

const postcss = require('gulp-postcss');

const uglify = require('gulp-uglify-es').default;

const fs = require('fs');

const json = JSON.parse(fs.readFileSync('./layerfile.json'));

const groupAggregate = require('gulp-group-aggregate');

const path = require('path');

const through = require('through2');

const copyAssetImg = function () {
    return through.obj(function (file, encoding, callback) {
        //copier les images des assets sans le rep généré
        const fileNoBase = file.dirname.replace(file.base, '');
        const regex = /\/([0-9a-z]+)\//;
        const fileNoAsset = fileNoBase.replace(regex, '');
        file.base = file.dirname.replace(fileNoAsset, '');
        callback(null, file);
    });
};

// Styles
task('stylespref', function () {

    function createErrorHandler(name) {
        return function (err) {
            console.error('Error from ' + name + ' in compress task', err.toString());
        };
    }

    json.layers.forEach(function (layer) {
        return src(layer.css)

            .pipe(postcss([autoprefixer()]))

            .on('error', createErrorHandler('autoprefixer'))

            .pipe(dest(`pixelhumain/ph/web/css/${layer.layerNameOut}/min`));

    });
    return Promise.resolve('the value is ignored');
});

task('styles', function () {

    function createErrorHandler(name) {
        return function (err) {
            console.error('Error from ' + name + ' in compress task', err.toString());
        };
    }

    json.layers.forEach(function (layer) {
        return src(layer.css)

            .pipe(postcss([autoprefixer()]))

            .on('error', createErrorHandler('autoprefixer'))

            .pipe(dest(`pixelhumain/ph/web/css/${layer.layerNameOut}/min`))

            .pipe(sourcemaps.init())

            .pipe(concat(`${layer.layerNameOut}.all.css`))

            .pipe(postcss([autoprefixer()]))

            .on('error', createErrorHandler('autoprefixer'))

            .pipe(dest(`pixelhumain/ph/web/css/${layer.layerNameOut}`))

            .pipe(rename({ suffix: '.min' }))

            .pipe(minifycss())

            .pipe(sourcemaps.write('.'))

            .pipe(dest(`pixelhumain/ph/web/css/`));

    });
    return Promise.resolve('the value is ignored');
});



task('scripts', function () {
    function createErrorHandler(name) {
        return function (err) {
            console.error('Error from ' + name + ' in compress task', err.toString());
        };
    }
    const retour = json.layers.map(function (layer) {
        return src(layer.js)

            //.pipe(jshint('.jshintrc'))

            //.pipe(jshint.reporter('default'))

            .pipe(sourcemaps.init())

            //.pipe(dest('pixelhumain/ph/web/js'))

            //.pipe(rename({ suffix: '.min' }))

            .pipe(concat(`${layer.layerNameOut}.all.js`))

            .pipe(sourcemaps.write())

            .pipe(dest('pixelhumain/ph/web/js'));

    });
    if (retour) {
        return retour[1];
    }
});

task('images', function () {

    return src('pixelhumain/ph/assets/**/**.{png,gif,jpg}')

        .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))

        .pipe(dest('pixelhumain/ph/assets/'))

    //.pipe(notify({ message: 'Images task complete' }));

});

task('imagesweb', function () {

    return src('pixelhumain/ph/assets/**/**.{png,gif,jpg}')

        //.pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))

        .pipe(copyAssetImg())

        .pipe(dest('pixelhumain/ph/web/'))

    //.pipe(notify({ message: 'Images task complete' }));

});


task('imageslightbox2web', function () {

    return src('pixelhumain/ph/plugins/lightbox2/img/*.{png,gif,jpg}')

        //.pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))

        //.pipe(copyAssetImg())

        .pipe(dest('pixelhumain/ph/web/img/'))

    //.pipe(notify({ message: 'Images task complete' }));

});

task('imagesfacemotion', function () {

    return src('pixelhumain/ph/plugins/facemotion/sqhTN9lgaY1.png')

        //.pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))

        //.pipe(copyAssetImg())

        .pipe(dest('pixelhumain/ph/web/css/'))

    //.pipe(notify({ message: 'Images task complete' }));

});

task('imagesfineuploader', function () {

    return src('pixelhumain/ph/plugins/fine-uploader/jquery.fine-uploader/*.{png,gif,jpg}')

        //.pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))

        //.pipe(copyAssetImg())

        .pipe(dest('pixelhumain/ph/web/css/'))

    //.pipe(notify({ message: 'Images task complete' }));

});

task('fonts', function () {

    return src(['pixelhumain/ph/plugins/font-awesome/fonts/*', 'pixelhumain/ph/plugins/bootstrap/fonts/*', 'pixelhumain/ph/themes/CO2/assets/fonts/**/*'])

        .pipe(dest('pixelhumain/ph/web/fonts'));

});

task('revisions', function () {

    return src(['pixelhumain/ph/web/**/*.all.min.*'])
        .pipe(revAll.revision())
        .pipe(dest('pixelhumain/ph/web/build'))
        .pipe(revAll.manifestFile())
        .pipe(dest('pixelhumain/ph/web/build'))
        .pipe(exec('rm -rf pixelhumain/ph/assets/*'))
        .pipe(exec('pixelhumain/ph/protected/yiic assets publish'));

});

task('replaceversion', function () {
    var tempsEnMs = Date.now();
    return src(["pixelhumain/ph/protected/config/paramsconfig.php"])
        .pipe(replace(/"versionAssets" \=\> "([0-9]+)"/g, '"versionAssets" => "' + tempsEnMs + '"'))
        .pipe(dest('pixelhumain/ph/protected/config/'))
        .pipe(exec('cp pixelhumain/ph/protected/config/paramsconfig.php pixelhumain/ph/protected/config/paramsconfig.old.php'))
        .pipe(exec('rm pixelhumain/ph/protected/config/paramsconfig.php'))
        .pipe(exec('cp pixelhumain/ph/protected/config/paramsconfig.old.php pixelhumain/ph/protected/config/paramsconfig.php'))
        .pipe(exec('rm pixelhumain/ph/protected/config/paramsconfig.old.php'))
});

task('clean', function (cb) {

    return del(['pixelhumain/ph/web/*']);

});

task('compress', function () {
    function createErrorHandler(name) {
        return function (err) {
            console.error('Error from ' + name + ' in compress task', err.toString());
        };
    }

    json.layers.map(function (layer) {
        return src(layer.js)

            //.pipe(jshint('.jshintrc'))

            //.pipe(jshint.reporter('default'))

            .pipe(sourcemaps.init())

            //.pipe(dest('pixelhumain/ph/web/js'))

            //.pipe(rename({ suffix: '.min' }))

            .pipe(concat(`${layer.layerNameOut}.all.js`))

            .pipe(sourcemaps.write())

            .pipe(dest('pixelhumain/ph/web/js'))

            //.pipe(notify({ message: 'Scripts task complete' }))

            //.pipe(src([`pixelhumain/ph/web/js/${layer.layerNameOut}.all.js`]))
            /*.pipe(babel({
                presets: ['@babel/preset-env']
            }))
            .on('error', createErrorHandler('babel'))*/
            .pipe(uglify())
            .on('error', createErrorHandler('uglify'))
            .pipe(rename({ suffix: '.min' }))
            .pipe(dest('pixelhumain/ph/web/js'));
    });
    return Promise.resolve('the value is ignored');
});

exports.build = series('clean', 'styles', 'compress', 'fonts', 'revisions', 'replaceversion', 'imagesweb', 'imageslightbox2web', 'imagesfacemotion','imagesfineuploader');
