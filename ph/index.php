<?php

if( stripos($_SERVER['SERVER_NAME'], "default") === false && 
	stripos($_SERVER['SERVER_NAME'], "127.0.0.1") === false && 
	stripos($_SERVER['SERVER_NAME'], "communecter56-dev") === false && 
	stripos($_SERVER['SERVER_NAME'], "communecter74-dev") === false && 
	stripos($_SERVER['SERVER_NAME'], "localhost") === false && 
	stripos($_SERVER['SERVER_NAME'], "::1") === false && 
	stripos($_SERVER['SERVER_NAME'], "localhost:8080") === false &&
	stripos($_SERVER['SERVER_NAME'], "localhost:5080") === false &&
	 strpos($_SERVER['SERVER_NAME'], "local.")!==0){
	defined('YII_DEBUG') or define('YII_DEBUG',false); // PROD
	error_reporting(0);
} else {
	error_reporting(E_ALL & ~E_WARNING);	
	ini_set('display_errors', 'On');
	defined('YII_DEBUG') or define('YII_DEBUG',true);//LOCAL DEV OR QA  	
}
// change the following paths if necessary
require_once(dirname(__FILE__) . '/vendor/autoload.php');

require dirname(__FILE__). '/protected/components/Yii.php';

// configuration for Yii 2 application
$yii2Config = require dirname(__FILE__) . '/protected/config/yii2/web.php';
new yii\web\Application($yii2Config); // Do NOT call run(), yii2 app is only used as service locator

// configuration for Yii 1 application
$yii1Config = require dirname(__FILE__) . '/protected/config/main.php';

//
//$yii=dirname(__FILE__).'/vendor/yiisoft/yii/framework/yii.php';
//$config=dirname(__FILE__).'/protected/config/main.php';
// remove the following lines when in production mode

    

// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

//require_once($yii);
//Yii::createWebApplication($config)->run();
Yii::createWebApplication($yii1Config)->run();