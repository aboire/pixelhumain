<style type="text/css">
<?php if(isset($this->costum) 
		&& isset($this->costum["css"]) 
		&& isset($this->costum["css"]["progress"])){
    		if(isset($this->costum["css"]["progress"]["bar"]) ){ ?>
    			.progressTop::-webkit-progress-bar { background: <?php echo $this->costum["css"]["progress"]["bar"]["background"] ?>;}
    		<?php }
    		if(isset($this->costum["css"]["progress"]["value"]) ){ ?>
        		.progressTop::-webkit-progress-value{background: <?php echo $this->costum["css"]["progress"]["value"]["background"] ?>;}
        		.progressTop::-moz-progress-bar{background: <?php echo $this->costum["css"]["progress"]["value"]["background"] ?>;}
        	<?php }
} ?>
</style>
<progress class="progressTop" max="100" value="20"></progress>   

<script type="text/javascript">
// console.log("render progress Bar","/pixelhumain/ph/themes/CO2/views/layouts/progressBar.php")
</script>