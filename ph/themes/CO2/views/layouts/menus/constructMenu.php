<?php 
	$idMenu=(isset($params["id"])) ? $params["id"] : @$key;
	$classMenu=(isset($params["class"])) ? $params["class"] : "";
	$classMenu.=(isset($params["addClass"])) ? " ".$params["addClass"] : "";
	$keyMenu=(isset($params["key"])) ? $params["key"] : @$key; 
	$commonButtonClass= (isset($params["buttonClass"])) ? $params["buttonClass"] : "";
?>
<div id="<?php echo $idMenu ?>" class="<?php echo $classMenu ?>">
<?php 

	if(!isset($params["buttonList"])){
		foreach($params as $key => $v){
			if(isset($v["menu"]) && $v["menu"]) {
				$this->renderPartial($layoutPath.'menus/constructMenu',array(
		            "params"=>$v,
		            "layoutPath"=>$layoutPath,
		            "sub"=>true ,
		            "key"=>$keyMenu              
	    		));
	    	}
		}
	}else{
		echo Menu::constructMenuBtn($params["buttonList"], $commonButtonClass);
	}
?>
</div>
<script type="text/javascript">
	
</script>