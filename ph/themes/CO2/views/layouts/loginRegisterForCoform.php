
<?php //HtmlHelper::registerCssAndScriptsFiles( array('/js/default/loginRegister.js') , $this->module->getParentAssetsUrl()); ?>

<style>
    .no-display {
        display: none;
    }
    .help-block{
        color:red;
    }
    .topLogoAnim{
        background-color: rgba(255, 255, 255, 0);
        position: absolute;
        z-index: 10;
        top: 65px !important;
        right: 13%;
        width: 400px;
        padding-left: 10px;
    }
    .topLogoAnim .homestead{
        font-size:41px !important;
        font-weight: 100 !important;
    }
    .titleWhite, .subTitle {
        color: #3C5665 !important;
    }
    .subTitle {
        font-weight: 300;
        font-size: 13px;
        margin-top: -15px !important;
    }
    @media (min-width: 768px) and (max-width: 1200px) {
        .topLogoAnim{
            top: 40px !important;
            right: 6%;
        }
        .topLogoAnim .homestead{
            font-size:32px !important;
        }
        .subTitle {
            font-size: 10px;
        }
    }
    @media (max-width: 768px){
        .topLogoAnim{
            top: 31% !important;
            right: 0% !important;
            width: 100%;

        }
        .topLogoAnim .homestead{
            font-size:18px !important;
        }
        .subTitle {
            font-size: 9px;
            margin-top: -3px !important;
        }
        .portfolio-modal.modal .name{
            letter-spacing: 2px;
        }
    }
</style>

<?php //if($subdomain != "welcome"){

$logo = (isset($this->costum["htmlConstruct"]["loadingModal"]["logo"])) ? $this->costum["htmlConstruct"]["loadingModal"]["logo"] : ((isset($this->costum["logo"])) ? $this->costum["logo"] : $this->module->getParentAssetsUrl()."/images/logoLTxt.jpg" );
if(@$logoNetwork && !empty($logoNetwork))
    $logo=$logoNetwork;
?>
<form class="portfolio-modal modal fade coform-login box-login" id="modalLogin" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="">

        <div class="container col-lg-offset-3 col-sm-offset-2 col-lg-6 col-sm-8 col-xs-12">
            <div class="col-xs-12 text-left">
                <label><i class="fa fa-envelope"></i> <?php echo Yii::t("login","Email") ?></label><br>
                <input class="form-control" name="email" id="email-login-coform" type="text" placeholder="<?php echo Yii::t("login","An email") ?>"><br>

                <label><i class="fa fa-key"></i> <?php echo Yii::t("login","Password") ?></label><br>
                <input class="form-control" name="password" id="password-login-coform" type="password" placeholder="<?php echo Yii::t("login","A password") ?>"><br>



                <label for="remember" class="checkbox-inline">
                    <input type="checkbox" id="remember" name="remember" checked="checked">
                    <?php echo Yii::t("login","Keep me signed in") ?>
                </label><br>
                <small><i class="fa fa-lock"></i> <?php echo Yii::t("login","password saved securely in your cookies") ?>.</small>
                <br><br>

                <button class="btn btn-success pull-right loginBtn" type="submit"><i class="fa fa-sign-in"></i> <?php echo Yii::t("login","Log in") ?></button>

                <div class="form-actions col-xs-12 no-padding" style="margin-top:20px;">
                    <div class="errorHandler alert alert-danger no-display loginResult">
                        <i class="fa fa-remove-sign"></i> <?php echo Yii::t("login","Please verify your entries.") ?>
                    </div>
                    <div class="alert alert-danger no-display notValidatedEmailResult text-center">
                        <i class="fa fa-remove-sign"></i><?php echo Yii::t("login","Your account <b>is not validated</b>: please check your mailbox to validate your email address.") ?><br/>
                        <?php echo Yii::t("login","If you <b>didn't receive it or lost it</b>, click on the <b>following button</b> to receive it <b>again</b>") ?><br/>
                        <a class="btn btn-default bg-white letter-blue bold margin-top-10" href="javascript:;" data-toggle="modal" data-target="#modalSendActivation"
                           onclick="$('#modalSendActivation #email2').val($('#email-login-coform').val());">
                            <i class="fa fa-envelope"></i> <?php echo Yii::t("login","Receive another validation email") ?>
                        </a>
                    </div>
                    <div class="alert alert-info no-display betaTestNotOpenResult">
                        <i class="fa fa-remove-sign"></i><?php echo Yii::t("login","Our developpers are fighting to open soon ! Check your mail that will happen soon !")?>
                    </div>
                    <div class="alert alert-success no-display emailValidated">
                        <i class="fa fa-check"></i> <?php echo Yii::t("login","Your account is now validated ! Please try to login.") ?>
                    </div>
                    <div class="alert alert-danger no-display custom-msg">
                        <i class="fa fa-remove-sign"></i> <?php echo Yii::t("login","You have some form errors. Please check below.") ?>
                    </div>
                    <div class="alert alert-danger no-display emailAndPassNotMatchResult">
                        <i class="fa fa-remove-sign"></i><?php echo Yii::t("login","Email or password does not match. Please try again !")?>
                    </div>
                    <div class="alert alert-danger no-display emailNotFoundResult">
                        <i class="fa fa-remove-sign"></i><?php echo Yii::t("login","Impossible to find an account for this username or password.")?>
                    </div>
                </div>

                <div class="col-xs-12 no-padding text-center">
                    <hr>
                    <a href="javascript:;" class="btn bg-white" data-toggle="modal" data-target="#modalForgot">
                        <!-- <i class="fa fa-s"></i> --><?php echo Yii::t("login","I forgot my password") ?>
                    </a>
                    <?php //if($subdomain != "welcome"){ ?>
                    <a href="javascript:;" class="btn btn-default bg-white letter-blue bold"
                       data-toggle="modal" data-target="#modalRegister">
                        <i class="fa fa-plus-circle bold"></i>
                        <?php echo Yii::t("login", "I create my account") ?>
                    </a>
                    <?php //}else{ ?>
                    <!-- <a href="javascript:;" class="btn btn-default bg-white letter-blue bold" data-dismiss="modal">
                                <i class="fa fa-plus-circle"></i>
                                <?php echo Yii::t("login", "I create my account") ?>
                        </a> -->
                    <?php //} ?>
                </div>

            </div>
        </div>
    </div>
</form>
<?php //} ?>

<!-- //RENDER PARTIAL MODAL REGISTER -->
<?php
if (@$this->costum && @$this->costum["register"]) {
    echo $this->renderPartial($this->costum["register"]);
}else{
    echo $this->renderPartial('webroot.themes.CO2.views.layouts.modalRegister');
}
?>

<script>
    var coformLogin = {
        //"use strict";
        rulesRegister : {
            name : {
                required : true,
                minlength : 4
            },
            username : {
                required : true,
                validUserName : true,
                rangelength : [4, 32]
            },
            email3 : {
                required : {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                email : true
            },
            password3 : {
                minlength : 8,
                required : true
            },
            passwordAgain : {
                equalTo : "#password3",
                required : true
            }
        },
        runBoxToShow : function() {
            var el = $('.box-login');
            if (coformLogin.getParameterByName('box').length) {
                switch(coformLogin.getParameterByName('box')) {
                    case "register" :
                        el = $('.box-register');
                        break;
                    case "password" :
                        el = $('.box-email');
                        emailType = 'password'
                        break;
                    case "validate" :
                        el = $('.box-email');
                        emailType = 'validateEmail'
                        break;
                    default :
                        el = $('.box-login');
                        break;
                }
            }
        },

        //function to return the querystring parameter with a given name.
        getParameterByName : function(name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(location.search);
            return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },
        runSetDefaultValidation : function() {
            $.validator.setDefaults({
                errorElement : "span", // contain the error msg in a small tag
                errorClass : 'help-block',
                errorPlacement : function(error, element) {// render error placement for each input type
                    if (element.attr("type") == "radio" || element.attr("type") == "checkbox") {
                        // for chosen elements, need to insert the error after the chosen container
                        error.insertAfter($(element).closest('.form-group').children('div').children().last());
                    } else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
                        error.appendTo($(element).closest('.form-group').children('div'));
                    } else {
                        error.insertAfter(element);
                        // for other inputs, just perform default behavior
                    }
                },
                ignore : ':hidden',
                success : function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error');
                },
                highlight : function(element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').addClass('has-error');
                    // add the Bootstrap error class to the control group
                },
                unhighlight : function(element) {// revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                }
            });
        },

        runLoginValidator : function() {
            var form = $('.coform-login');
            var loginBtn = null;
            form.submit(function(e){e.preventDefault() });
            var errorHandler = $('.errorHandler', form);
            form.validate({
                rules : {
                    email : {
                        minlength : 2,
                        required : true
                    },
                    password : {
                        minlength : 4,
                        required : true
                    }
                },
                submitHandler : function(form) {
                    errorHandler.hide();
                    $(".alert").hide();
                    $(".loginBtn").find(".fa").removeClass("fa-sign-in").addClass("fa-spinner fa-spin");
                    var params = {
                        "email" : $("#email-login-coform").val(),
                        "pwd" : $("#password-login-coform").val(),
                        "remember" : $("#formLogin #remember").prop("checked")
                    };
                    mylog.log("lazyLoadMany params", params);
                    ajaxPost(
                        null,
                        baseUrl+"/"+moduleId+"/person/authenticate",
                        params,
                        function(data){
                            if(data.result){
                                if($("#remember").prop("checked")){
                                    var pwdEncrypt = encryptPwd($("#password-login-coform").val());
                                    var emailEncrypt = encryptPwd($("#email-login-coform").val());
                                    $.cookie("lyame", emailEncrypt, { expires: 180, path : "/" });
                                    $.cookie("drowsp", pwdEncrypt, { expires: 180, path : "/" });
                                    $.cookie("remember", $("#remember").prop("checked"), { expires: 180, path : "/" });
                                }
                                var url = requestedUrl;
                                if(data.goto != null){
                                    window.location.href = baseUrl+"/"+moduleId+data.goto;
                                } else if( typeof dyFObj.openFormAfterLogin != "undefined"){
                                    userId = data.id;
                                    $('#modalLogin').modal("hide");
                                    dyFObj.openForm( dyFObj.openFormAfterLogin.type, dyFObj.openFormAfterLogin.afterLoad, dyFObj.openFormAfterLogin.data );
                                } else {
                                    userId=data.id;
                                    var hash = location.hash.replace( "#","" );
                                    if(typeof hash != "undefined" && hash != ""){
                                        var hashT=hash.split(".");
                                        if(typeof hashT == "string")
                                            var slug=hashT;
                                        else
                                            var slug=hashT[0];
                                        ajaxPost(
                                            null,
                                            baseUrl+"/"+moduleId+"/slug/check",
                                            {slug:slug},
                                            function(data){
                                                if (!data.result)
                                                    window.location.reload();
                                                else{
                                                    if(location.hash.indexOf("#page") >= 0)
                                                        window.location.reload();
                                                    else
                                                        window.location.reload();
                                                }
                                            },
                                            function(data) {
                                                mylog.log("error"); mylog.dir(data)
                                            }
                                        );
                                    }else{
                                        if(location.hash.indexOf("#page") >= 0)
                                            window.location.reload();
                                        else
                                            window.location.reload();
                                    }
                                }
                            } else {
                                var msg;
                                if (data.msg == "notValidatedEmail") {
                                    $('.notValidatedEmailResult').show();
                                } else if (data.msg == "betaTestNotOpen") {
                                    $('.betaTestNotOpenResult').show();
                                } else if (data.msg == "emailNotFound") {
                                    $('.emailNotFoundResult').show();
                                } else if (data.msg == "emailAndPassNotMatch") {
                                    $('.emailAndPassNotMatchResult').show();
                                } else if (data.msg == "accountPending") {
                                    pendingUserId = data.pendingUserId;
                                    $(".errorHandler").hide();
                                    //$('.register').trigger("click");
                                    $('.pendingProcess').show();
                                    var pendingUserEmail = data.pendingUserEmail;
                                    $('#email3').val(pendingUserEmail);
                                    $('#email3').prop('disabled', true);
                                    $('.form-register #isInvitation').val(true);
                                    $('#modalRegister').modal("show");
                                } else{
                                    msg = data.msg;
                                    $('.loginResult').html(msg);
                                    $('.loginResult').show();
                                }
                                $(".loginBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
                                //loginBtn.stop();
                            }
                        },
                        function(data) {
                            mylog.log(data);
                            $(".loginBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
                            toastr.error("Something went really bad : contact your administrator !");
                            //loginBtn.stop();
                        }
                    );
                    return false; // required to block normal submit since you used ajax
                },
                invalidHandler : function(event, validator) {//display error alert on form submit
                    $(".loginBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
                    errorHandler.show();
                    //loginBtn.stop();
                }
            });
        },

        runForgotValidator : function() {
            var form2 = $('.form-email');
            var errorHandler2 = $('.errorHandler', form2);
            var forgotBtn = null;
            Ladda.bind('.forgotBtn', {
                callback: function (instance) {
                    forgotBtn = instance;
                }
            });
            form2.validate({
                rules : {
                    email2 : {
                        required : true
                    }
                },
                submitHandler : function(form) {
                    errorHandler2.hide();
                    forgotBtn.start();
                    var params = {
                        "email" : $("#email2").val(),
                        "type"	: "password"
                    };
                    ajaxPost(
                        null,
                        baseUrl+"/"+moduleId+"/person/sendemail",
                        params,
                        function(data){
                            if (data.result) {
                                $('.modal').modal('hide');
                                $("#modalNewPasswordSuccess").modal("show");
                                // Hide modal if "Okay" is pressed
                                $('#modalNewPasswordSuccess .btn-default').click(function() {
                                    $('.modal').modal('hide');
                                });
                            } else if (data.errId == "UNKNOWN_ACCOUNT_ID") {
                                toastr.error(data.msg);
                                $(".forgotBtn").prop("disabled", false).data("loading", false);
                                forgotBtn.stop();
                            }
                        },
                        function(data) {
                            toastr.error("Something went really bad : contact your administrator !");
                        }

                    );
                    return false;
                },
                invalidHandler : function(event, validator) {//display error alert on form submit
                    errorHandler2.show();
                    forgotBtn.stop();
                }
            });
        },
        runEmailValidationValidator : function() {
            var form2 = $('.form-email-activation');
            var errorHandler2 = $('.errorHandler', form2);
            var sendValidateEmailBtn = null;
            Ladda.bind('.sendValidateEmailBtn', {
                callback: function (instance) {
                    sendValidateEmailBtn = instance;
                }
            });
            form2.validate({
                rules : {
                    email2 : {
                        required : true
                    }
                },
                submitHandler : function(form) {
                    errorHandler2.hide();
                    sendValidateEmailBtn.start();
                    var params = {
                        "email" : $("#modalSendActivation #email2").val(),
                        "type"	: "validateEmail"
                    };
                    ajaxPost(
                        null,
                        baseUrl+"/"+moduleId+"/person/sendemail",
                        params,
                        function(data){
                            if (data.result) {
                                $('.modal').modal('hide');
                                $("#modalSendAgainSuccess").modal("show");
                                // Hide modal if "Okay" is pressed
                                $('#modalSendAgainSuccess .btn-default').click(function() {
                                    $('.modal').modal('hide');
                                });
                            } else if (data.errId == "UNKNOWN_ACCOUNT_ID") {
                                toastr.error(data.msg);
                                $(".sendValidateEmailBtn").prop("disabled", false).data("loading", false);
                                sendValidateEmailBtn.stop();
                            }
                        },
                        function(data) {
                            toastr.error("Something went really bad : contact your administrator !");
                        }

                    );
                    return false;
                },
                invalidHandler : function(event, validator) {//display error alert on form submit
                    errorHandler2.show();
                    sendValidateEmailBtn.stop();
                }
            });
        },
        addPostalCodeInRegister : function(){
            if($(".form-register .localityRegister").length <= 0){
                strLoc='<div class="localityRegister"><br/>';
                if(coformLogin.registerOptions.addressForm.streetAddress){
                    strLoc+='<div class="addressLOcalityRegister">'+
                        '<label class="letter-black"><i class="fa fa-address-book-o"></i> Rue</label>'+
                        '<input class="form-control" id="streetAddress" name="streetAddress" type="text" placeholder="Saisir la rue et le numéro">'+
                        '<div class="dropdown-register-addresses"></div>'+
                        '<br/>'+
                        '</div>';
                }
                if(coformLogin.registerOptions.addressForm.postalCode){
                    strLoc+='<div class="postalCodeRegister">'+
                        '<label class="letter-black"><i class="fa fa-address-book-o"></i> Code postal</label>'+
                        '<input class="form-control" id="postalCode" name="postalCode" type="text" placeholder="Saisir un code postal">'+
                        '<br/>'+
                        '</div>';
                }
                if(coformLogin.registerOptions.addressForm.addressLocality){
                    strLoc+='<div class="cityRegister">'+
                        '<label class="letter-black"><i class="fa fa-address-book-o"></i> Ville</label>'+
                        '<input class="form-control" id="addressLocality" name="addressLocality" type="text" placeholder="Saisir une ville">'+
                        '<br/>'+
                        '</div>';
                }
                strLoc+="</div>";
                $('.form-register').find(coformLogin.registerOptions.addressPosition).after(strLoc);
            }
        },
        addressFormat : function(params){
            if($(".form-register .localityRegister").length > 0){
                if($('.form-register #postalCode').val() != "" && $('.form-register #addressLocality').val() != ""){
                    params.address={
                        "postalCode": $('.form-register #postalCode').val(),
                        "addressLocality": $('.form-register #addressLocality').val()
                    };
                    if($(".form-register #streetAddress").length > 0 && $('.form-register #streetAddress').val() != ""){
                        params.address.streetAddress=$('.form-register #streetAddress').val();
                    }
                }

            }
            return params;
        },
        formatDataRegister : function(){
            var params = {
                "name" : $('.form-register #registerName').val(),
                "username" :$(".form-register #username").val(),
                "email" : $(".form-register #email3").val(),
                "pwd" : $(".form-register #password3").val(),
                "app" : moduleId, //"$this->module->id"
                "pendingUserId" : pendingUserId,
                "mode" : coformLogin.registerOptions.mode
            };
            if(coformLogin.registerOptions.loginAfter)
                params.loginAfter=true;
            if($('.form-register #isInvitation').val())
                params.isInvitation=true;
            if( $("#inviteCode").val() )
                params.inviteCode = $("#inviteCode").val();
            params=coformLogin.addressFormat(params);
            return params;
        },
        registerOptions : {
            mode : "two_steps_register",
            loginAfter : false,
            addressForm : {
                streetAddress : true,
                postalCode : true,
                addressLocality : true
            },
            addressPosition : ".passwordAgainRegister"
        },
        //alert("incorun run");
        runRegisterValidator : function() {
            mylog.log("runRegisterValidator!!!!");
            //alert("runRegisterValidator 1");
            var form3 = $('.form-register');
            var errorHandler3 = $('.errorHandler', form3);
            var createBtn = null;
            if(coformLogin.registerOptions.mode=="normal"){
                coformLogin.addPostalCodeInRegister();
            }
            form3.validate({
                rules : coformLogin.rulesRegister,
                submitHandler : function(form) {
                    //alert("runRegisterValidator 2");
                    mylog.log("runRegisterValidator submitHandler");
                    if(!form3.find("#agree").is(":checked")){
                        var validator = $( '.form-register' ).validate();
                        validator.showErrors({
                            "agree": trad["mustacceptCGU"]
                        });
                        return false;
                    }
                    errorHandler3.hide();
                    //createBtn.start();
                    $(".createBtn").prop('disabled', true);
                    $(".createBtn").find(".fa").removeClass("fa-sign-in").addClass("fa-spinner fa-spin");
                    params=coformLogin.formatDataRegister();
                    ajaxPost(
                        null,
                        baseUrl+"/"+moduleId+"/person/register",
                        params,
                        function(data){
                            if(data.result) {
                                //createBtn.stop();
                                $(".createBtn").prop('disabled', false);
                                $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
                                $("#registerName").val("");
                                $("#username").val("");
                                $("#email3").val("");
                                $("#password3").val("");
                                $("#passwordAgain").val("");
                                $(".form-register #addressLocality").val("");
                                $(".form-register #postalCode").val("");
                                $(".form-register #city").val("");
                                $('#agree').prop('checked', false);
                                mylog.log("authenticate",data);
                                if((typeof data.isInvitation != "undefined" && data.isInvitation) || coformLogin.registerOptions.loginAfter){
                                    toastr.success(data.msg);
                                    if(typeof themeParams.pages["#app.index"].redirect != "undefined"
                                        && typeof themeParams.pages["#app.index"].redirect.register != "undefined"){
                                        history.pushState(null, "New Title","#"+themeParams.pages["#app.index"].redirect.register);
                                        window.location.reload();
                                    }else{
                                        history.pushState(null, "New Title",'#page.type.citoyens.id.'+params.pendingUserId);
                                        //window.location.href = baseUrl+'#page.type.citoyens.id.'+data.id;
                                        window.location.reload();
                                    }

                                }
                                else{
                                    $('.modal').modal('hide');
                                    $("#modalRegisterSuccessContent").html("<h3><i class='fa fa-smile-o fa-4x text-green'></i><br><br> "+data.msg+"</h3>");
                                    $("#modalRegisterSuccess").modal({ show: 'true' });
                                    // Hide modal if "Okay" is pressed
                                    $('#modalRegisterSuccess .btn-default').click(function() {
                                        mylog.log("hide modale and reload");
                                        $('.modal').modal('hide');
                                        //window.location.href = baseUrl+'/#default.live';
                                        window.location.href = baseUrl+"/"+moduleId;
                                        window.location.reload();
                                    });
                                }
                            }else {
                                $('.registerResult').html(data.msg);
                                $('.registerResult').show();
                                $(".createBtn").prop('disabled', false);
                                $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
                            }
                        },
                        function(data) {
                            toastr.error(trad["somethingwentwrong"]);
                            $(".createBtn").prop('disabled', false);
                            $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
                        }

                    );
                    return false;
                },
                invalidHandler : function(event, validator) {//display error alert on form submit
                    errorHandler3.show();
                    $(".createBtn").prop('disabled', false);
                    $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
                    //createBtn.stop();
                }
            });
        },
        //return {
        loaded : false,
        //main function to initiate template pages
        init : function() {

            mylog.log("init after register");
            var ListPath = [
                //tka todo : should be loaded on demand
                //'/plugins/jquery.dynForm.js',
                //'/plugins/jquery-validation/dist/jquery.validate.min.js',
                '/plugins/jQuery-Knob/js/jquery.knob.js',
                //'/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
                //'/plugins/jquery.dynSurvey/jquery.dynSurvey.js',

                //'/plugins/select2/select2.min.js' ,
                //'/plugins/moment/min/moment.min.js' ,
                //'/plugins/moment/min/moment-with-locales.min.js',

                // '/plugins/bootbox/bootbox.min.js' ,
                // '/plugins/blockUI/jquery.blockUI.js' ,
                //'/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js' ,
                //'/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css',
                //'/plugins/jquery-cookieDirective/jquery.cookiesdirective.js' ,
                //'/plugins/ladda-bootstrap/dist/spin.min.js' ,
                //'/plugins/ladda-bootstrap/dist/ladda.min.js' ,
                //'/plugins/ladda-bootstrap/dist/ladda.min.css',
                //'/plugins/ladda-bootstrap/dist/ladda-themeless.min.css',
                //'/plugins/animate.css/animate.min.css'
            ];

            lazyLoadMany( ListPath,
                function() {
                    mylog.log("lazyLoadMany count",countLazyLoad, ListPath.length);
                    if(countLazyLoad == ListPath.length){
                        //alert("List Loaded play callback");
                        addCustomValidators();
                        coformLogin.runBoxToShow();
                        coformLogin.runSetDefaultValidation();
                        coformLogin.runLoginValidator();
                        coformLogin.runForgotValidator();
                        coformLogin.runEmailValidationValidator();
                        // alert("HERE");
                        //coformLogin.runRegisterValidator();
                        coformLogin.loaded = true;
                    }
                });

        },
        openLogin : function() {
            //alert("openLogin");
            if(!Login.loaded)
                Login.init();
            $('#modalLogin').modal("show");
            //$('#modalRegister').modal("show");
        }
        //}
    }

    var email = '<?php echo @$_GET["email"]; ?>';
    var userValidated = '<?php echo @$_GET["userValidated"]; ?>';
    var pendingUserId = '<?php echo @$_GET["pendingUserId"]; ?>';
    var name = '<?php echo @$_GET["name"]; ?>';
    var error = '<?php echo @$_GET["error"]; ?>';
    var invitor = "<?php echo @$_GET["invitor"]?>";

    var msgError = {
        "accountAlreadyExists" : "<?php echo Yii::t("login","Your account already exists on the plateform : please try to login.") ?>",
        "unknwonInvitor" : "<?php echo Yii::t("login","Something went wrong ! Impossible to retrieve your invitor.") ?>",
        "somethingWrong" : "<?php echo Yii::t("login","Something went wrong !") ?>",
    };

    var buttonLabel = {
        "password" : '<?php echo Yii::t("login","Get my password") ?>',
        "validateEmail" : "<?php echo Yii::t("login","Send me validation email") ?>"
    };

    var timeout;
    var emailType;


    var requestedUrl = "<?php echo (isset(Yii::app()->session["requestedUrl"])) ? Yii::app()->session["requestedUrl"] : null; ?>";
    var REGISTER_MODE_TWO_STEPS = "<?php echo Person::REGISTER_MODE_TWO_STEPS ?>";

    jQuery(document).ready(function() {
        //Remove parameters from URLs in case of invitation without reloading the page
        <?php if(@$_GET["email"]){
        if(@$_GET["el"]){ ?>
        removeParametersWithoutReloading("<?php echo $_GET["el"] ?>");
        <?php }else{ ?>
        removeParametersWithoutReloading();
        <?php }
        } ?>
        //alert("HERE");
        if(!userConnected)
            coformLogin.init();
        else
            addCustomValidators();

        $("#modalRegister").on('show.bs.modal', function(){
            // alert('The modal is fully shown.');
            coformLogin.runRegisterValidator();
        });
        $('.form-register #username').keyup(function(e) {
            validateUserName();
        });

        if(typeof initLoginRegister != "undefined" &&  initLoginRegister.email != ''){
            $('#email-login-coform').val(initLoginRegister.email);
            $('#email-login-coform').prop('disabled', true);
            $('#email2').val(initLoginRegister.email);
            $('#email2').prop('disabled', true);
            $('#email3').val(initLoginRegister.email);
            $('#email3').prop('disabled', true);
        }

        //Validation of the user (invitation or validation)
        userValidatedActions();

        if (error != "") {
            $(".custom-msg").show();
            $(".custom-msg").text(msgError[error]);
        }

        /*$("#username").change(function(){
            $("#registerName").val($(this).val());
        });*/

    });


</script>