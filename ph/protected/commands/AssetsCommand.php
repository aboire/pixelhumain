<?php

class AssetsCommand extends CConsoleCommand {
    #protected/yiic assets publish
    public function actionPublish() {
        $folder = dirname(__FILE__).'/../../../../modules/';
        $scanned_directory = array_diff(scandir($folder), array('..', '.'));
        foreach($scanned_directory as $subfolder){
            if(is_dir($folder.$subfolder.'/assets/')){
                Yii::app()->assetManager->publish($folder.$subfolder.'/assets/');
            }
        }
    }
}