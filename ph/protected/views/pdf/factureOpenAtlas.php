<style type="text/Css">

.trHeaderTable{
	text-align: center; 
	border-top: solid 1px #E5596D; 
	border-bottom: solid 1px #E5596D; 
	height: 20px;
	color: #E5596D;
}

.backgroudRed{
	background-color: #fafafa;
	opacity: 0.1;
}

.tdTableWhite{
	text-align: center;
	height: 20px;
	padding: 2px;
	font-size: 11pt;
}

.textColor{
	color: #E5596D;
}

tr{
	padding-top: 10px !important;
	padding-bottom: 10px !important;
}

</style>

<page backtop="7mm" backbottom="7mm" backleft="10mm" backright="10mm"> 
	
	<page_body>
		<table style="width :100%;">
			<tr>
				<td>
					<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/OpenAtlas/logo-openatlas.png" alt="Logo" width="150" />
				</td>
				<td> </td>
			</tr>
			<tr>
				<td>
					<br/>
					<div style="font-weight: bold;"> <?php echo $organization["name"] ?></div> <br>
					<span style="font-size: 12px;">
						<?php echo isset($organization["adresse"])?$organization["adresse"]."<br>":"" ?> 
						56 rue Andy Saint-Paul, <br>
						&nbsp;&nbsp;97422 SAINT-PAUL<br>
						<?php echo "&nbsp;&nbsp;<b>Email : </b>".$organization["email"] ?>
					</span>
				</td>
				<td style="border: solid 1mm #E5596D; border-radius: 10px;">
					<br/>
					<div style="font-weight: bold;"> Adresse de facturation : </div>
					<div style="font-size: 12px; margin-left: 40px">
						<?php
							echo "&nbsp;&nbsp;".$associe["name"]."<br>";
							echo "&nbsp;&nbsp;".$associe["email"]."<br>";
							//echo "&nbsp;&nbsp;".$currency."<br>";
							//print_r($payment);
						?>
					</div>
				</td>
			</tr>
		</table>
		<br/><br/><br/>
		<!--div style="text-align: right; margin: auto">A <?php // echo $params["customerAddress"]["addressLocality"]; ?> le <?php // echo date("d/m/Y", strtotime($params["orderDate"])); ?></div><br/-->
		<div style="text-align: left; margin: auto; font-weight: bold; font-size: 16px">Facture : <?php  echo date("Y-m-d")."_Facture_".$payment["payment"]["id"]; ?></div><br/>
		<br/>
		<table style="width :100%;">
			<tr>
				<th class="trHeaderTable w40">DESCRIPTIF</th>
				<th class="trHeaderTable w20">QTE</th>
				<th class="trHeaderTable w20">PU HT</th>
				<th class="trHeaderTable w20">PRIX HT</th>
			</tr>
			<tr>
				<?php 
				if(!empty($payment)){
				//	foreach ($params["orderItems"] as $key => $value) {
					echo '<td class="tdTableWhite backgroudRed" style="text-align: left"> <br/> <br/>'.$payment["payment"]["description"].'</td>';
					echo '<td class="tdTableWhite backgroudRed"> <br/> <br/>'.$payment["payment"]["nbPart"].'</td>';
					echo '<td class="tdTableWhite backgroudRed"> <br/> <br/>'.$payment["payment"]["value"]/$payment["payment"]["nbPart"].' '.$currency.'</td>';
					echo '<td class="tdTableWhite backgroudRed"> <br/> <br/>'.$payment["payment"]["value"].' '.$currency.'</td>';
				//	}
				} ?>
			</tr>
		</table>
		<br/><br/><br/>
		<table style="width :500px;">
			<tr>
				<th class="trHeaderTable " style="text-align: center;">TOTAL TTC</th>
				<td class="trHeaderTable " style="text-align: left;"></td>
				<td class="trHeaderTable " style="text-align: left;"></td>
				<td class="trHeaderTable " style="text-align: center;"> <?php echo $payment["payment"]['value'].' '.$currency ?> </td>
			</tr>
		</table>
	<page_body>  
	<page_footer> 
		<br/><br/><br/>
		<hr style="background: black;">
		<div style="text-align: center; margin: auto; font-size: 14px">
			TVA  non applicable, article 293 B du Code général des impôts <br/><br/>
			<a href="www.memoire-numerique.org">
				<?php //print_r($payment); ?>
			</a>

		</div>

		<div style="color: #aaaaaa; font-size: 12px">

			Conditions de paiement :
			<br>
Règlement à effectuer sur le compte bancaire :<br>
Code Banque: 10107<br>
Code guichet: 00737<br>
BIC: BREDFRPPXXX<br>
Numéro de compte: 00537014829 Clé: 53<br>
IBAN: FR76 1010 7007 3700 5370 1482 953<br>
Domiciliation: THALES TECHNOPOLE
		</div>
	</page_footer> 
</page>