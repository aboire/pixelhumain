<?php return array(
"Problem inserting the new person : "=>"Težava z vstavljanjem nove osebe :",
"is missing"=>"manjka",
"Problem inserting the new person : email is not well formated"=>"Težava z vstavljanjem nove osebe : e-pošta ni dobro oblikovana",
"Problem inserting the new person : a person with this email already exists in the plateform"=>"Težava z vstavljanjem nove osebe: oseba s tem e-poštnim naslovom že obstaja v platformi",
"Problem inserting the new person : a person with this username already exists in the plateform"=>"Težava z vstavljanjem nove osebe : oseba s tem uporabniškim imenom že obstaja v platformi",
"Problem inserting the new person : unknown city"=>"Težava z vstavljanjem nove osebe : neznano mesto",
"Find people you know by name or email"=>"Poiščite ljudi, ki jih poznate po imenu ali e-pošti",
"Add a Person"=>"Dodajanje osebe",
"Your current password is incorrect"=>"Vaše trenutno geslo je napačno.",
"The new password should be 8 caracters long"=>"Novo geslo mora biti dolgo 8 znakov.",
"Your password has been changed with success !"=>"Vaše geslo je bilo uspešno spremenjeno !",
"Sorry, you can't invite more people to join the platform. You do not have enough invitations left."=>"Žal ne morete povabiti več ljudi, da se pridružijo platformi. Imate premalo povabil.",
"The person has been updated"=>"Oseba je bila posodobljena",
"Birth date"=>"Datum rojstva",
"Phone"=>"Telefon",
"Mobile"=>"Mobilni",
"Country"=>"Država",
"Postal Code"=>"Poštna številka",
"Address"=>"Naslov",
"Fax"=>"Faks",
"Username"=>"Uporabniško ime",
"Short Description"=>"Kratek opis",
"Contact information"=>"Kontaktni podatki",
"My space"=>"Moj prostor",
"My city"=>"Moje mesto",
"My Votes / Discussions"=>"Moji glasovi / Razprave",
"Invite someone"=>"Povabite nekoga",
"Create an event"=>"Ustvarjanje dogodka",
"Create a project"=>"Ustvarite projekt",
"Create an organization"=>"Ustvarite organizacijo",
"Sign out"=>"Odjavite se",
"Edit your informations"=>"Uredite svoje podatke",
"Enter your name"=>"Vnesite svoje ime",
"Enter your user name"=>"Vnesite svoje uporabniško ime",
"Enter your phones"=>"Vnesite svoje telefone",
"Enter your mobiles"=>"Vnesite svoje mobilne telefone",
"Enter your fax"=>"Vnesite svoj faks",
"Hello, \nCome and meet me on that website!\nAn email, your town and you are connected to your city!\nYou can see everything that happens in your city and act for the commons."=>"Pozdravljeni, \nPridi in me spoznaj na tej spletni strani!\nE-pošta, vaše mesto in vi ste povezani s svojim mestom!\nMorete videti vse, kar se dogaja v vašem mestu, in delovati za skupnosti.",
); ?>