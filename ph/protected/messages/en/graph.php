<?php
return array(
    "circle" => "circle",
    "Circle" => "Circle",
    "mindmap" => "mindmap",
    "Mindmap" => "Mindmap",
    "relation" => "relation",
    "Relation" => "Relation",
    "network" => "network",
    "Network" => "Network",
    "Doughnut" => "Doughnut",
    "doughnut" => "doughnut"
);