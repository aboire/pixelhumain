<?php
return array(
    "circle" => "kreis",
    "Circle" => "Kreis",
    "mindmap" => "mindmap",
    "Mindmap" => "Mindmap",
    "relation" => "beziehung",
    "Relation" => "Beziehung",
    "network" => "netzwerk",
    "Network" => "Netzwerk",
    "Doughnut" => "Krapfen",
    "doughnut" => "krapfen"
);