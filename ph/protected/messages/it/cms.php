<?php
	return array(
		"Menu construction"=>"Costruzione del menu",
		"Add navigation bar"=>"Aggiungi barra di navigazione",
		"Right menu"=>"Menu a destra",
		"Left menu"=>"Menu a sinistra",
		"Top left menu"=>"Menù in alto a sinistra",
		"Top right menu"=>"Menù in alto a destra",
		"Menu at the bottom"=>"Menù in basso",
		"Manage existing"=>"Gestisci esistente",
		"Build your menu"=>"Costruisci il tuo menu",
	)
?>
