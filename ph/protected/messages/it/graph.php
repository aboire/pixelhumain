<?php
return array(
    "circle" => "cerchio",
    "Circle" => "Cerchio",
    "mindmap" => "mappa mentale",
    "Mindmap" => "Mappa mentale",
    "relation" => "relazione",
    "Relation" => "Relazione",
    "network" => "rete",
    "Network" => "Rete",
    "doughnut" => "ciambella",
    "Doughnut" => "Ciambella"
);