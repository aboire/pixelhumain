<?php return array(
"Loading"=>"Laddar",
"Thanks to be patient a moment"=>"Tack för att du har tålamod ett ögonblick",
"1 little coin to contribute"=>"1 litet mynt att bidra med",
"Help us to forward our work"=>"Hjälp oss att föra vårt arbete framåt",
"More informations about our financial situation"=>"Mer information om vår ekonomiska situation",
"Make a donation to the NGO"=>"Ge en donation till den icke-statliga organisationen",
"1 little coin for a great puzzle"=>"1 litet mynt för ett stort pussel",
"Contribute"=>"Bidra till",
"Member ( 1€/month )"=>"Medlem ( 1€/månad )",
"Sponsor ( 50€/month )"=>"Sponsor ( 50€/månad )",
"Make a donation"=>"Gör en donation",
); ?>