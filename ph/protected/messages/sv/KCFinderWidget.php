<?php return array(
"You need to set a upload directory and URL.<br/>Check the documentation."=>"Du måste ange en uppladdningskatalog och URL.<br/>Kontrollera dokumentationen.",
"Set the user Id to use this type of storage"=>"Ange användar-ID för att använda den här typen av lagring.",
"Set the CKEditor container to initialize this widget"=>"Ställ in CKEditor-behållaren för att initiera den här widgeten",
); ?>