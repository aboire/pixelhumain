<?php return array(
"Menu construction"=>"Meny för konstruktion",
"Add navigation bar"=>"Lägg till navigeringsfältet",
"Right menu"=>"Höger meny",
"Left menu"=>"Vänster meny",
"Top left menu"=>"Meny längst upp till vänster",
"Top right menu"=>"Meny längst upp till höger",
"Menu at the bottom"=>"Meny längst ner",
"Manage existing"=>"Hantera befintliga",
"Build your menu"=>"Skapa din meny",
); ?>