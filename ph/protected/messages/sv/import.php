<?php return array(
"001"=>"001",
"100"=>"100",
"101"=>"101",
"102"=>"102",
"103"=>"103",
"104"=>"104",
"105"=>"105",
"106"=>"106",
"110"=>"110",
"111"=>"111",
"112"=>"112",
"150"=>"150",
"151"=>"151",
"152"=>"152",
"153"=>"153",
"154"=>"154",
"170"=>"170",
"171"=>"171",
"172"=>"172",
"201"=>"201",
"202"=>"202",
"203"=>"203",
"204"=>"204",
"205"=>"205",
"206"=>"206",
"207"=>"207",
"208"=>"208",
"209"=>"209",
"210"=>"210",
"211"=>"211",
"212"=>"212",
"250"=>"250",
"300"=>"300",
"add my mapping."=>"lägga till min kartläggning.",
"Grab the name your mapping"=>"Ta namnet på din kartläggning",
"Add my mapping"=>"Lägg till min kartläggning",
"Add the mapping, this allow you to reuse"=>"Lägg till kartläggningen, så att du kan återanvända",
"update my mapping"=>"uppdatera min kartläggning",
"Do you want to change name of your mapping ?"=>"Vill du ändra namnet på din kartläggning?",
"Name your mapping : "=>"Namnge din kartläggning :",
"Update my mapping"=>"Uppdatera min kartläggning",
"delete my mapping"=>"radera min kartläggning",
"Are you sure of delete your mapping ?"=>"Är du säker på att du har raderat din kartläggning?",
"You will not be able to use this mapping"=>"Du kommer inte att kunna använda den här kartläggningen.",
"Yes, I confirm the delete my mapping"=>"Ja, jag bekräftar att jag raderar min mappning",
"Documentation"=>"Dokumentation",
"Key : "=>"Nyckel :",
"Key assigned to all data import"=>"Nyckel som tilldelas alla importerade uppgifter",
"Number of entites to test (max 900) : "=>"Antal enheter som ska testas (max 900) :",
"Delete my mapping"=>"Ta bort min mappning",
"Author Invite: "=>"Författare inbjudna:",
"Message Invite"=>"Meddelande Inbjudan",
"List of elements :"=>"Förteckning över element :",
"List of cities has add :"=>"Förteckningen över städer har lagts till :",
"Imported data : "=>"Importerade uppgifter :",
"Data rejected : "=>"Avvisade uppgifter :",
"Return"=>"Återge",
"Save"=>"Spara",
"Page add of datas"=>"Sida med tillägg av uppgifter",
"Data sheet referenced"=>"Hänvisat datablad",
"You will need to enter the name for your mapping"=>"Du måste ange namnet på din mappning.",
"An error occurred"=>"Ett fel inträffade",
"Your mapping has been delete"=>"Din kartläggning har tagits bort",
"You will need to select an element type"=>"Du måste välja en elementtyp",
"You will need to select an source"=>"Du måste välja en källa",
"You will need to select an file"=>"Du måste välja en fil",
"You have already added this elements in the CSV column"=>"Du har redan lagt till dessa element i CSV-kolumnen",
"You have already add this elements in the mapping column"=>"Du har redan lagt till dessa element i mappningskolumnen",
"You will need to add the Key"=>"Du måste lägga till nyckeln",
"You must make at least one data assignment"=>"Du måste göra minst en datainsamling.",
"You will need to add the elements in the mapping"=>"Du måste lägga till följande element i mappningen",
"You will need to select a file CSV, JSON or XML"=>"Du måste välja en fil CSV, JSON eller XML.",
" element(s)"=>" element",
"There is "=>"Det finns",
"Your mapping have been added"=>"Din kartläggning har lagts till",
"Your mapping have been updated"=>"Din kartläggning har uppdaterats",
"You will need to complete at least one field"=>"Du måste fylla i minst ett fält",
"Add"=>"Lägg till",
"Fields mandatory"=>"Obligatoriska fält",
"Grap manually my mapping"=>"Grap manuellt min kartläggning",
"Other"=>"Övriga",
"Upload CSV, JSON or XML"=>"Ladda upp CSV, JSON eller XML",
"File (CSV, JSON, XML)"=>"Fil (CSV, JSON, XML)",
"You will need to fill field"=>"Du måste fylla i fältet",
"You can't to fill the source Other"=>"Du kan inte fylla källan Annat",
"Source"=>"Källa",
"Result"=>"Resultat",
); ?>