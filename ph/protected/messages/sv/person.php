<?php return array(
"Problem inserting the new person : "=>"Problem med att infoga den nya personen :",
"is missing"=>"saknas.",
"Problem inserting the new person : email is not well formated"=>"Problem med att infoga den nya personen: e-postmeddelandet är inte välformulerat.",
"Problem inserting the new person : a person with this email already exists in the plateform"=>"Problem med att infoga den nya personen: en person med denna e-postadress finns redan i plattformen.",
"Problem inserting the new person : a person with this username already exists in the plateform"=>"Problem med att lägga in den nya personen: en person med detta användarnamn finns redan på plattformen.",
"Problem inserting the new person : unknown city"=>"Problem med att infoga den nya personen : okänd stad",
"Find people you know by name or email"=>"Hitta personer som du känner till via namn eller e-post",
"Add a Person"=>"Lägg till en person",
"Your current password is incorrect"=>"Ditt nuvarande lösenord är felaktigt",
"The new password should be 8 caracters long"=>"Det nya lösenordet ska vara 8 tecken långt",
"Your password has been changed with success !"=>"Ditt lösenord har ändrats med framgång!",
"Sorry, you can't invite more people to join the platform. You do not have enough invitations left."=>"Tyvärr kan du inte bjuda in fler personer till plattformen. Du har inte tillräckligt med inbjudningar kvar.",
"The person has been updated"=>"Personen har uppdaterats",
"Birth date"=>"Födelsedatum",
"Phone"=>"Telefon",
"Mobile"=>"Mobil",
"Country"=>"Land",
"Postal Code"=>"Postnummer",
"Address"=>"Adress",
"Fax"=>"Fax",
"Username"=>"Användarnamn",
"Short Description"=>"Kort beskrivning",
"Contact information"=>"Kontaktuppgifter",
"My space"=>"Mitt utrymme",
"My city"=>"Min stad",
"My Votes / Discussions"=>"Mina röster / Diskussioner",
"Invite someone"=>"Bjuda in någon",
"Create an event"=>"Skapa en händelse",
"Create a project"=>"Skapa ett projekt",
"Create an organization"=>"Skapa en organisation",
"Sign out"=>"Logga ut",
"Edit your informations"=>"Redigera dina uppgifter",
"Enter your name"=>"Ange ditt namn",
"Enter your user name"=>"Ange ditt användarnamn",
"Enter your phones"=>"Ange dina telefoner",
"Enter your mobiles"=>"Ange dina mobiler",
"Enter your fax"=>"Ange ditt fax",
"Hello, \nCome and meet me on that website!\nAn email, your town and you are connected to your city!\nYou can see everything that happens in your city and act for the commons."=>"Hej, \nKom och träffa mig på den där webbplatsen!\nEn e-post, din stad och du är ansluten till din stad!\nDu kan se allt som händer i din stad och agera för allmänningarna.",
); ?>