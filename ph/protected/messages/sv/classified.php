<?php return array(
"Send a private message to the advertiser"=>"Skicka ett privat meddelande till annonsören",
"classified published by"=>"klassificerad och utgiven av",
"Add a classified"=>"Lägg till en klassificerad",
"Publish"=>"Publicera",
"{what} published by {who}"=>"{vad} publicerat av {vem}",
"{what} published by"=>"{vad} publiceras av",
); ?>