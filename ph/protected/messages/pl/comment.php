<?php return array(
"Comments"=>"Uwagi",
"comments"=>"uwagi",
"comment"=>"jak",
"answers"=>"odpowiada",
"answer"=>"odpowiedź",
"Popular"=>"Popularny",
"Abuse"=>"Nadużycia",
"Say Something"=>"Say Something",
"You are going to declare this comment as abuse : please fill the reason ?"=>"Zamierza Pan uznać ten komentarz za nadużycie: proszę podać powód ?",
"Please fill a reason"=>"Proszę wpisać powód",
"You already declare this comment as abused."=>"Już uznajesz ten komentarz za nadużyty.",
"Error calling the serveur : contact your administrator."=>"Błąd wywołania serwitora: skontaktuj się z administratorem.",
"Validate"=>"Zatwierdzić",
"Cancel"=>"Anuluj",
"The comment has been posted"=>"Komentarz został zamieszczony",
"Something went really bad"=>"Coś poszło bardzo źle",
"Agree with that"=>"Zgadzam się z tym",
"Disagree with that"=>"Nie zgadzam się z tym",
"Report an abuse"=>"Zgłoś nadużycie",
"This comment has been deleted because of his content."=>"Ten komentarz został usunięty z powodu jego treści.",
"You are going to delete this comment : are your sure ?"=>"Zamierzasz usunąć ten komentarz: czy jesteś pewien?",
"No comment"=>"Bez komentarza",
"Update your comment"=>"Zaktualizuj swój komentarz",
"Show more comments"=>"Pokaż więcej komentarzy",
"Your comment is empty"=>"Twój komentarz jest pusty",
"Do you want to delete this comment"=>"Czy chcesz usunąć ten komentarz?",
"Loading comments"=>"Ładowanie komentarzy",
); ?>