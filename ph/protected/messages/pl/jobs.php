<?php return array(
"The job posting id is mandatory to retrieve the job posting !"=>"Identyfikator oferty pracy jest obowiązkowy, aby pobrać ofertę pracy!",
"New Job Offer"=>"Nowa oferta pracy",
"Fill the form"=>"Wypełnij formularz",
"Job Posting"=>"pośrednictwo pracy",
"Your job offer has been updated with success"=>"Twoja oferta pracy została zaktualizowana z sukcesem",
"Can not insert the job : unknown field "=>"Nie można wstawić zlecenia: nieznane pole",
"Problem inserting the new job offer"=>"Problem z wstawieniem nowej oferty pracy",
"Your job offer has been added with succes"=>"Twoja oferta pracy została dodana z sukcesem",
"Can not update the job : you are not authorized to update that job offer !"=>"Nie można zaktualizować oferty pracy : Nie masz uprawnień do aktualizacji tej oferty pracy !",
"Error updating the Organization : address is not well formated !"=>"Błąd aktualizacji Organizacji : adres nie jest dobrze sformowany !",
"Can not remove the job : you are not authorized to update that job offer !"=>"Nie można usunąć pracy: nie masz uprawnień do aktualizacji tej oferty pracy!",
"Your job offer has been deleted with success"=>"Twoja oferta pracy została usunięta z sukcesem",
); ?>