<?php
	return array(
		"Loading"=>"Cargando",
		"Thanks to be patient a moment"=>"Agradecemos su paciencia",
		"1 little coin to contribute"=>"una pequeña moneda para colaborar",
		"Help us to forward our work" =>"Ayúdanos a reenviar nuestro trabajo",
		"More informations about our financial situation"=>"Más información sobre nuestra situación financiera",
		"Make a donation to the NGO"=>"Hacer una donación al negocio",
		"1 little coin for a great puzzle"=>"1 pequeña moneda para un gran rompecabezas",
		"Contribute"=>"Contribuir",
		"Member ( 1€/month )"=>"Miembro ( 1€/mes)",
		"Sponsor ( 50€/month )"=>"Patrocinador ( 50€/mes )",
		"Make a donation"=>"Hacer una donación",		
	);

?>