<?php

return array(
        'Problem inserting the new person : ' => "Problema al insertar la nueva persona:",
        'is missing' => 'está faltando',
        'Problem inserting the new person : email is not well formated' => "Problema al insertar la nueva persona: el correo electrónico no está bien formateado",
        "Problem inserting the new person : a person with this email already exists in the plateform" => "Problema al insertar la nueva persona: ya existe una persona con éste correo electrónico en la plataforma",
        "Problem inserting the new person : a person with this username already exists in the plateform" => "Problema al insertar la nueva persona: ya existe una persona con este nombre de usuario en la plataforma",
        "Problem inserting the new person : unknown city" => "Problema al insertar la nueva persona: ciudad desconocida",
        'Find people you know by name or email' => 'Encuentra a personas que conoces por nombre o correo electrónico',
        'Add a Person' => 'Añadir una persona',
        //Change Password
        "Your current password is incorrect" => "Tu contraseña actual es incorrecta.",
        "The new password should be 8 caracters long" => "La nueva contraseña debe tener 8 caracteres.",
        "Your password has been changed with success !" => "¡Tu contraseña ha sido cambiada con éxito!",
        //Invitation process
        "Sorry, you can't invite more people to join the platform. You do not have enough invitations left." => "Lo sentimos, no puedes invitar a más personas a unirse a la plataforma. No te quedan suficientes invitaciones.",
        "The person has been updated" => "La persona ha sido actualizada.",
        "Birth date" => "Fecha de nacimiento",
        "Phone" => "Teléfono",
        "Mobile" => "Celular",
        "Country" => "País",
        "Postal Code" => "Codigo Postal",
        "Address" => "Drección",
        "Fax" => "Fax",
        "Username" => "Nombre de usuario",
        "Short Description" => "Breve descripción",
        "Contact information" => "Información de contacto",
        "My space" => "Mi espacio",
        "My city" => "Mi ciudad",
        "My Votes / Discussions" => "Mis Votos / Discusiones",
        "Invite someone" => "Invita a alguien",
        "Create an event" => "Crear un evento",
        "Create a project" => "Crear un proyecto",
        "Create an organization" => "Crear una organización",
        "Sign out" => "Desconectar",
        "Edit your informations" => "Edita tu información",
        "Enter your name" => "Ingresa tu nombre",
        "Enter your user name" => "Ingresa tu nombre de usuario",
        "Enter your phones" => "Ingresa tus telefonos",
        "Enter your mobiles" => "negreas tu celular",
        "Enter your fax" => "Ingresa tu fax",
        "Hello, \\nCome and meet me on that website!\\nAn email, your town and you are connected to your city!\\nYou can see everything that happens in your city and act for the commons." => "Hola, \\n¡Ven a conocerme en ese sitio! \\nUn correo electrónico, tu ciudad y ¡estás conectado a tu ciudad!. Puedes ver todo lo que sucede en tu ciudad y actuar para los comunes.\\n"	
);

?>