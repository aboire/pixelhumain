<?php
return array(
    "circle" => "circulo",
    "Circle" => "Circulo",
    "mindmap" => "mapa mental",
    "Mindmap" => "Mapa mental",
    "relation" => "relación",
    "Relation" => "Relación",
    "network" => "red",
    "Network" => "Red",
    "Doughnut" => "Rosquilla",
    "doughnut" => "rosquilla"
);