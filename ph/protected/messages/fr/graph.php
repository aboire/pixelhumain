<?php
return array(
    "circle" => "cercle",
    "Circle" => "Cercle",
    "mindmap" => "carte de pensée",
    "Mindmap" => "Carte de pensée",
    "relation" => "relation",
    "Relation" => "Relation",
    "network" => "réseau",
    "Network" => "Réseau",
    "Doughnut" => "Donut",
    "doughnut" => "donut"
);